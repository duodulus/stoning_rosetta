
function CB() {
  var cb = new Codebird;
  cb.setConsumerKey("6KEstaAZkkgWR4HC7F3Ss9iZc",
                    "PrOaV8Kk99BkQfa0yyh3PFCNiOviNBO3L4s8DUCD9SkprmMa5r");
  return cb;
}

function twLogin() {

  var cb = CB();

  // gets a request token
  cb.__call(
    "oauth_requestToken",
    {}, //{oauth_callback: "oob"},
    function (reply) {
      console.log(reply);
      
      // stores it
      cb.setToken(reply.oauth_token, reply.oauth_token_secret);
      localStorage.setItem('token', reply.oauth_token);
      localStorage.setItem('token_secret', reply.oauth_token_secret);
      
      // gets the authorize screen URL
      cb.__call(
        "oauth_authorize",
        {},
        function (auth_url) {
          window.codebird_auth = window.open(auth_url);
        }
      );
    }
  );
  
}

// ready function
(function(){
  var query       = location.search.substr(1).split("&");
  var parameters  = {};
  var parameter;
  
  for (var i = 0; i < query.length; i++) {
    parameter = query[i].split("=");
    if (parameter.length === 1) {
      parameter[1] = "";
    }
    parameters[decodeURIComponent(parameter[0])] = decodeURIComponent(parameter[1]);
  }
  
  // check if oauth_verifier is set by callback function
  if (typeof parameters.oauth_verifier !== "undefined"  && localStorage.getItem('token')) {
    var cb = CB();

    cb.setToken(localStorage.getItem('token'), localStorage.getItem('token_secret'));

    cb.__call(
      "oauth_accessToken",
      {
        oauth_verifier: parameters.oauth_verifier
      },
      function (reply) {
        if (reply.httpstatus == 200) {
          localStorage.setItem('user', reply.screen_name);
          localStorage.setItem('uid', reply.user_id);
          localStorage.setItem('login', 'tw');
          location.pathname = 'game'
        }
      }
    );    
  }
    
})();
