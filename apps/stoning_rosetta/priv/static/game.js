var renderer = PIXI.autoDetectRenderer(800, 600);
document.body.appendChild(renderer.view);

var stage = new PIXI.Container();

var bg = PIXI.Sprite.fromImage('/static/images/brain-export.jpg');
stage.addChild(bg);

var count = 0;


var style = {
    font : "32px 'Fjalla One'",
    fill : '#e5d37e', //fae484',
    //stroke : '#4a1850',
    strokeThickness : 0,
    //dropShadow : true,
    //dropShadowColor : '#000000',
    //dropShadowAngle : Math.PI / 6,
    //dropShadowDistance : 6,
    wordWrap : true,
    wordWrapWidth : 440
};



animate();

function animate() {

    count += 0.005;

    // render the root container
    renderer.render(stage);

    requestAnimationFrame(animate);
}
