(function(window){

  "use strict";

  var S = {
    SELECT_BASE_LANG: 1,
    SELECT_LEARN_LANG: 2,
    SELECT_PROFESSOR: 3,
    LOGIN: 4,
  };
  
  var state = S.SELECT_BASE_LANG;

  var context = {
    baselang: null,
    learnlang: null,
    professor: null,
  }
  
  // let's handle all events here.
  document.addEventListener('click', function(e) {
    var prev = state;
    if (state == S.SELECT_BASE_LANG) {
      if (e.target != null && e.target.dataset != null && e.target.dataset.baselang != null) {
        context.baselang = e.target.dataset.baselang;
        state = S.SELECT_LEARN_LANG;
      }
    } else if (state == S.SELECT_LEARN_LANG) {
      if (e.target != null && e.target.dataset != null && e.target.dataset.learnlang != null) {
        context.learnlang = e.target.dataset.learnlang;
        state = S.SELECT_PROFESSOR;
      } else { // back button
        state = S.SELECT_BASE_LANG;
      }
    } else if (state == S.SELECT_PROFESSOR) {
      document.getElementsByClassName('slides')[0].className.replace( /(?:^|\s)third(?!\S)/g , 'fourth')
      state = S.LOGIN;
    }
    show_state(prev, state);
    e.target.blur();
  });

  function show_state(prev, next) {
    // login
    document.getElementsByTagName('header')[0].style.visibility = (next == S.SELECT_BASE_LANG ? '': 'hidden');

    // roll the slides
    var elem = document.getElementsByClassName('slides')[0];
    if (prev == S.SELECT_BASE_LANG) {
      if (next == S.SELECT_LEARN_LANG) {
        elem.className = elem.className.replace( /first/g , 'second');
      } else if (next == S.LOGIN) {
        elem.className = elem.className.replace( /first/g , 'second');
      }
    }
    
  }
  

})(window)
