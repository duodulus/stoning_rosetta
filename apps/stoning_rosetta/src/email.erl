-module(email).
-compile(export_all).


%init(Req0=#{method := <<"GET">>}, State, _Opts) ->
%        Req = cowboy_req:reply(200, #{
%                <<"content-type">> => <<"text/plain">>
%        }, <<"Hello world!">>, Req0),
%        {ok, Req, State};
%init(Req0, State, _Opts) ->
%        Req = cowboy_req:reply(405, #{
%                <<"allow">> => <<"GET">>
%        }, Req0),
%        {ok, Req, State}.

init(Trans, Req, _Opts) ->
   eaws_client:connect(),
   {ok, Req, []}.

terminate(_Reason, _Req, _State) -> ok.

handle(Req, State) ->
        {Email, Req2}    = cowboy_req:qs_val(<<"email">>, Req, ""),
        Salt     = case application:get_env(stoning_rosetta, email_salt) of
                      undefined -> "Hölkyn klönkyn";
                      {ok, V1}  -> V1
                   end,
        io:format("salt ~p ~p\n", [Salt, Email]),
        Uniq = crypto:hash(sha, binary_to_list(Email) ++ Salt),
        io:format("uniq ~p\n", [Uniq]),

        eaws_client:connect(),
        eaws_client:send_formatted_email([{to_addresses, [binary_to_list(Email)]}, {from_address, "noreply@stoningrosetta.cf"}, {subject, "Stoning Rosetta"}, {text_body, "Here is a login link for your game: " ++ binary_to_list(Uniq) }, {html_body, ""}]),

        Resp = cowboy_req:set_resp_header(<<"content-type">>, <<"text/plain">>, Req2),
        Resp2 = cowboy_req:set_resp_body(<<"This is test">>, Resp),
        Resp3 = cowboy_req:reply(200, Resp2),
        {ok, Resp3, State}.



% eaws_client:connect(),
% eaws_client:send_formatted_email([{to_addresses, ["matti.katila@gmail.com"]}, {from_address, "matti.katila@gmail.com"}, {subject, "test"}, {text_body, "daah"}, {html_body, ""}]).