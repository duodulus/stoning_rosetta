touch docker.zip
rm docker.zip
zip -r docker.zip deps
zip -r docker.zip apps
zip -g docker.zip mad
zip -g docker.zip rebar.config
zip -g docker.zip sys.config
zip -g docker.zip vm.args
zip -g docker.zip Dockerfile
zip -g docker.zip Dockerrun.aws.json
find deps | grep '.git' | xargs zip -d docker.zip
