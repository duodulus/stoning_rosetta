#FROM msaraiva/erlang:18.1
FROM erlang:19.0

#RUN apk --update add erlang erlang-crypto erlang-syntax-tools erlang-inets inotify-tools-dev && rm -rf /var/cache/apk/*
# RUN apk --update add alpine-sdk #fortify-headers #alpine-sdk
# RUN apk --update add musl-dev #fortify-headers #alpine-sdk
# RUN apk --update add gcc #linux-headers 
#RUN apk add 'erlang=19.0.1-r0' --no-cache --repository http://nl.alpinelinux.org/alpine/edge/community

COPY deps /app/deps
COPY apps /app/apps
COPY rebar.config /app/rebar.config
COPY vm.args /app/vm.args
COPY sys.config /app/sys.config
COPY mad /app/mad

EXPOSE 8001
CMD cd /app/ && ./mad repl

