
window.fbAsyncInit = function() {
  FB.init({
    appId      : '928154917313165',
    xfbml      : true,
    version    : 'v2.7'
  });
};

// Load the SDK asynchronously
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


// This function is called when someone finishes with the Login
// Button.  See the onlogin handler attached to it in the sample
// code below.
function fbLogin() {
  FB.login(function(response){
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      FB.api('/me', function(data) {
        localStorage.setItem('user', data.name);
        localStorage.setItem('uid', data.id);
        localStorage.setItem('login', 'fb');
        location.pathname = 'game'
      });

    } else if (response.status === 'not_authorized') {
      alert('Please authorize the application to play');
    }
  });
}


